﻿using System;
using System.Collections.Generic;
using UnityEngine;
using WordConnect;

public class GameManager : BaseManager
{
    #region Enums

    public enum EndGameType
    {
        Win,
        Death
    }

    #endregion Enums

    #region Inspector variables

#pragma warning disable
    [SerializeField] private FireballObject fireballObject;
    [SerializeField] private AudioSource menuGameSound;
#pragma warning restore

    #endregion Inspector variables

    #region Private variables

    private readonly Dictionary<EndGameType, Action> endGameActions = new Dictionary<EndGameType, Action>();

    private ObjectPool fireballObjectPool;

    #endregion Private variables

    #region Properties

    public override Type ManagerType => typeof(GameManager);

    public FireballObject FireballObject => fireballObject;

    public ObjectPool FireballObjectPool => fireballObjectPool;

    public List<CandleController> candlesList { get; private set; } = new List<CandleController>();

    #endregion Properties

    #region Unity functions

    private void Awake()
    {
        InstanceManager.AddManager(this);
        endGameActions.Add(EndGameType.Death, GameOver);
        endGameActions.Add(EndGameType.Win, WinGame);
    }

    private void Start()
    {
        fireballObjectPool = new ObjectPool(fireballObject.gameObject, 5, ObjectPool.CreatePoolContainer(transform, "fireball_item_pool_container"));
    }

    #endregion Unity functions

    #region Public functions

    public void EndGame(EndGameType endGameType)
    {
        endGameActions[endGameType]();
        Time.timeScale = 0f;
    }

    public void AddCandle(CandleController candle)
    {
        if (!candlesList.Contains(candle))
        {
            candlesList.Add(candle);
        }
    }

    public void RemoveCandle(CandleController candle)
    {
        if (candlesList.Contains(candle))
        {
            candlesList.Remove(candle);
        }
        if (candlesList.Count == 0)
        {
            EndGame(EndGameType.Win);
        }
    }

    #endregion Public functions

    #region Private functions

    private void WinGame()
    {
        Debug.Log("Win game");
        menuGameSound.Stop();
        AudioManager.Instance.Play("win");
        InstanceManager.GetManager<PopupManager>().Show("win");
    }

    private void GameOver()
    {
        Debug.Log("Game over");
        menuGameSound.Stop();
        AudioManager.Instance.Play("dead");
        InstanceManager.GetManager<PopupManager>().Show("lose");
    }

    #endregion Private functions
}