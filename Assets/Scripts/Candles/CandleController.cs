﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class CandleController : MonoBehaviour
{
#pragma warning disable
    [SerializeField] private GameObject messageText;
    [SerializeField] private SpriteRenderer fireSprite;
#pragma warning restore

    private bool isSave = false;

    private void Start()
    {
        InstanceManager.GetManager<GameManager>().AddCandle(this);
    }

    private void ShowPleasureText()
    {
        fireSprite.gameObject.SetActive(false);
        messageText.SetActive(true);
    }

    public void SaveMe()
    {
        if (!isSave)
        {
            StartCoroutine(Save());
        }
    }

    private IEnumerator Save()
    {
        isSave = true;
        AudioManager.Instance.Play("fireout");
        ShowPleasureText();
        yield return new WaitForSeconds(2f);
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        InstanceManager.GetManager<GameManager>().RemoveCandle(this);
    }
}