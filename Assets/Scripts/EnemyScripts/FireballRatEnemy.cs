﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballRatEnemy : BaseEnemy
{
    [SerializeField] private float shootRepeatTime = 2f;

    public Transform Player { get { return InstanceManager.GetManager<PlayerManager>().PlayerHealth.transform; } }

    public override void Attack(int damage = 0)
    {
        //throw fireball
        var fireball = InstanceManager.GetManager<GameManager>().FireballObjectPool.GetObject<FireballObject>(transform);
        fireball.Throw(transform.position, Player.position);
    }

    protected override IEnumerator Move()
    {
        while (true)
        {
            if (IsMoveToPlayer)
            {
                this.Attack();
                animator.SetTrigger("Shoot");
                yield return new WaitForSeconds(shootRepeatTime);
            }
            yield return null;
        }
    }

    public override void Die()
    {
    }
}