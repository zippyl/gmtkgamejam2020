﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballObject : MonoBehaviour
{
#pragma warning disable
    [SerializeField] private AnimationCurve animationCurve;
    [SerializeField] private float moveSpeed;
#pragma warning restore

    private Coroutine fireballCoroutine;

    private Vector2 pos;
    private float time;

    public void Throw(Vector2 startPoint, Vector2 endPoint)
    {
        fireballCoroutine = StartCoroutine(ThrowCoroutine(startPoint, endPoint));
    }

    private IEnumerator ThrowCoroutine(Vector2 startPoint, Vector2 endPosition)
    {
        while (true)
        {
            time += Time.deltaTime * moveSpeed;
            pos = Vector3.Lerp(startPoint, endPosition, time);
            pos.y += animationCurve.Evaluate(time);
            transform.position = pos;
            if (time >= 0.95f)
            {
                InstanceManager.GetManager<GameManager>().FireballObjectPool.ReturnObjectToPool(gameObject);
                StopCoroutine(fireballCoroutine);
                pos = Vector2.zero;
                time = 0;
            }
            yield return null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            time = 0;
            pos = Vector2.zero;
            StopCoroutine(fireballCoroutine);
            InstanceManager.GetManager<PlayerManager>().FireUP();
            InstanceManager.GetManager<GameManager>().FireballObjectPool.ReturnObjectToPool(gameObject);
        }
    }
}