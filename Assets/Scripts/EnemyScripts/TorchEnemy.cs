﻿using UnityEngine;

public class TorchEnemy : BaseEnemy
{
    public override void Attack(int damage = 20)
    {
        base.Attack(damage);
        InstanceManager.GetManager<PlayerManager>().FireUP();
    }

    public override void Die()
    {
        base.Die();
        AudioManager.Instance.Play("fireout");
    }
}