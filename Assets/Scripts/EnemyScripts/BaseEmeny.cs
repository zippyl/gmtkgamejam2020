﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class OurPoint
{
    public float startPoint;
    public float endPoint;

    public float GetSub { get { return endPoint - startPoint; } }
}

public class BaseEnemy : MonoBehaviour
{
#pragma warning disable
    [SerializeField] protected OurPoint movePoint;
    [SerializeField] protected float enemyMoveSpeed = 1;
    [SerializeField] protected float speedMultiplier = 2;
    [SerializeField] protected SpriteRenderer spriteRenderer;
    [HideInInspector, SerializeField] private PlayerManager playerManager;
#pragma warning restore

    protected Coroutine moveMe;
    protected Transform playerTransform;
    protected bool dirRight = true;
    protected Animator animator;

    public OurPoint MovePoint => movePoint;

    public bool IsMoveToPlayer { get; private set; } = false;

    public virtual void Start()
    {
        moveMe = StartCoroutine(Move());
        animator = GetComponent<Animator>();
    }

    private void OnValidate()
    {
        if (playerManager == null) playerManager = FindObjectOfType<PlayerManager>();
        Debug.Assert(playerManager != null, $"{typeof(PlayerManager).Name} is not present");
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Attack();
        }
    }

    protected virtual IEnumerator Move()
    {
        while (true)
        {
            if (IsMoveToPlayer)
            {
                transform.position = Vector3.Lerp(transform.position, new Vector2(playerTransform.position.x, transform.position.y), Time.deltaTime * speedMultiplier);
            }
            else
            {
                if (dirRight)
                {
                    spriteRenderer.flipX = false;
                    transform.Translate(Vector2.right * enemyMoveSpeed * Time.deltaTime);
                }
                else
                {
                    spriteRenderer.flipX = true;
                    transform.Translate(-Vector2.right * enemyMoveSpeed * Time.deltaTime);
                }

                if (transform.position.x >= movePoint.endPoint)
                {
                    dirRight = false;
                }

                if (transform.position.x <= movePoint.startPoint)
                {
                    dirRight = true;
                }
            }
            yield return null;
        }
    }

    public virtual void Die()
    {
        StopCoroutine(moveMe);
        gameObject.SetActive(false);
    }

    public virtual void Attack(int damage = 0)
    {
        playerManager.DealDamage(damage);
    }

    public void SetMoveToPlayerState(bool state, Transform transform)
    {
        IsMoveToPlayer = state;
        playerTransform = transform;
    }
}