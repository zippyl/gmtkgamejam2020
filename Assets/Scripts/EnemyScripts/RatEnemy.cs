﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatEnemy : BaseEnemy
{
    public override void Attack(int damage = 0)
    {
        base.Attack(damage);
        InstanceManager.GetManager<PlayerManager>().FireUP();
    }

    public override void Die()
    {
    }
}