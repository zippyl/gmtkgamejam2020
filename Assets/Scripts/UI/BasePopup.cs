﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePopup : MonoBehaviour
{
    #region Protected functions

    //this functions is actualy non-protected

    public void Show()
    {
        gameObject.SetActive(true);

        OnShowing();
    }

    public void Hide()
    {
        gameObject.SetActive(false);

        OnHiding();
    }

    #endregion Protected functions

    #region Public functions

    //if you use this class it's ok, but if you not use this class, but use only the inherits, you should make functions below "abstract" and make class abstract too

    public virtual void OnShowing()
    {
    }

    public virtual void OnHiding()
    {
    }

    #endregion Public functions
}