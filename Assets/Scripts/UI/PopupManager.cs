﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public sealed class PopupManager : BaseManager
{
    #region Infos

    [Serializable]
    public class PopupInfos
    {
        public string ID;
        public BasePopup basePopup;
    }

    #endregion Infos

    #region Inspector variables

#pragma warning disable

    [SerializeField] private List<PopupInfos> popupInfos;

#pragma warning restore

    #endregion Inspector variables

    #region Unity functions

    private void Awake()
    {
        InstanceManager.AddManager(this);
    }

    #endregion Unity functions

    #region Private variables

    private BasePopup currentPopup;

    #endregion Private variables

    #region Public functions

    public void Show(string id)
    {
        currentPopup = popupInfos.Where(x => x.ID == id).First().basePopup;
        currentPopup.Show();
    }

    public void CloseCurrentPopup()
    {
        if (currentPopup != null)
        {
            currentPopup.Hide();
        }
    }

    #endregion Public functions

    public override Type ManagerType => typeof(PopupManager);
}