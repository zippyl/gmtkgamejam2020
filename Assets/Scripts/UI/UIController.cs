﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIController : BaseManager
{
    #region Inspector variables

#pragma warning disable
    [SerializeField] private Slider hpSlider;
#pragma warning restore

    #endregion Inspector variables

    #region Properties

    public override Type ManagerType => typeof(UIController);

    #endregion Properties

    #region Unity functions

    private void Awake()
    {
        InstanceManager.AddManager(this);
    }

    private void Start()
    {
        hpSlider.maxValue = InstanceManager.GetManager<PlayerManager>().DeathTime;
        hpSlider.value = InstanceManager.GetManager<PlayerManager>().DeathTime;
    }

    #endregion Unity functions

    #region Public functions

    public void VisualizeHP(float hp)
    {
        hpSlider.value = hp;
    }

    #endregion Public functions
}