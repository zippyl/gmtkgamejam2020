﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : BaseManager
{
    #region Inspector variables

#pragma warning disable
    [HideInInspector, SerializeField] private PlayerHealth playerHealth;
    [HideInInspector, SerializeField] private PlayerObstacle playerObstacle;
    [HideInInspector, SerializeField] private PlayerMovement playerMovement;

    [SerializeField] private Slider timeSlider;
    [SerializeField] private float deathTime = 40f;
#pragma warning restore

    #endregion Inspector variables

    #region Private variables

    private Coroutine firedUpCoroutine;

    #endregion Private variables

    #region Properties

    public override Type ManagerType => typeof(PlayerManager);

    public PlayerHealth PlayerHealth => playerHealth;

    public float DeathTime => deathTime;

    #endregion Properties

    #region Unity functions

    private void Awake()
    {
        InstanceManager.AddManager(this);
    }

    private void OnValidate()
    {
        if (playerHealth == null) playerHealth = FindObjectOfType<PlayerHealth>();
        if (playerObstacle == null) playerObstacle = FindObjectOfType<PlayerObstacle>();
        if (playerMovement == null) playerMovement = FindObjectOfType<PlayerMovement>();
        Debug.Assert(playerHealth != null, $"{typeof(PlayerHealth).Name} is not present");
        Debug.Assert(playerObstacle != null, $"{typeof(PlayerObstacle).Name} is not present");
        Debug.Assert(playerMovement != null, $"{typeof(PlayerMovement).Name} is not present");
    }

    #endregion Unity functions

    #region Public functions

    public void DealDamage(int damage = 20)
    {
        AudioManager.Instance.Play("dealdamage");
        playerHealth.DealDamage(damage);
    }

    public void FireUP()
    {
        Debug.Log("Fire up");
        firedUpCoroutine = StartCoroutine(IamFired());
    }

    private IEnumerator IamFired()
    {
        InstanceManager.GetManager<PlayerManager>().playerMovement.SetFireState(true);
        while (deathTime > 0)
        {
            InstanceManager.GetManager<UIController>().VisualizeHP(deathTime -= Time.deltaTime);
            yield return null;
        }
        yield return new WaitForSeconds(deathTime);
        InstanceManager.GetManager<GameManager>().EndGame(GameManager.EndGameType.Death);
    }

    public void FireOut()
    {
        Debug.Log("Fire out");
        InstanceManager.GetManager<PlayerManager>().playerMovement.SetFireState(false);
        if (firedUpCoroutine != null)
        {
            StopCoroutine(firedUpCoroutine);
        }
    }

    #endregion Public functions
}