﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    #region Properties

    public int HP { get; private set; } = 100;

    public int MaxHP { get; private set; }

    #endregion Properties

    #region Unity functions

    private void Awake()
    {
        MaxHP = HP;
    }

    #endregion Unity functions

    #region Public functions

    public void DealDamage(int damage = 20)
    {
        HP -= damage;
        if (HP <= 0)
        {
            HP = 0;
            DiePlease();
        }
        InstanceManager.GetManager<UIController>().VisualizeHP(HP);
        Debug.Log($"{HP} is player health");
    }

    #endregion Public functions

    #region Private functions

    private void DiePlease()
    {
        Time.timeScale = 0f;
        InstanceManager.GetManager<GameManager>().EndGame(GameManager.EndGameType.Death);
    }

    #endregion Private functions
}