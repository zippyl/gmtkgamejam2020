﻿using System;
using System.Collections;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
#pragma warning disable
    [SerializeField] private float speed;
    [SerializeField] private float jumpForce;
    [SerializeField] private SpriteRenderer fireRenderer;
#pragma warning restore

    private float distToGround;
    private Coroutine moveCoroutine;
    private Rigidbody2D playerRig;
    private SpriteRenderer playerRenderer;
    private Collider2D playerCollider;
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
        playerCollider = GetComponent<Collider2D>();
        playerRenderer = GetComponent<SpriteRenderer>();
        playerRig = GetComponent<Rigidbody2D>();
        distToGround = playerCollider.bounds.extents.y;
        StartCoroutine(Move());
    }

    private IEnumerator Move()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                playerRig.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            }

            float axis = Input.GetAxis("Horizontal");

            if (axis < 0)
            {
                playerRenderer.flipX = true;
                fireRenderer.flipX = true;
            }
            else
            {
                playerRenderer.flipX = false;
                fireRenderer.flipX = false;
            }
            animator.SetFloat("Blend", axis);
            Vector3 tempVect = new Vector3(axis * speed * Time.deltaTime, 0, 0);
            transform.position += tempVect;
            yield return null;
        }
    }

    public void SetFireState(bool state)
    {
        fireRenderer.gameObject.SetActive(state);
    }
}