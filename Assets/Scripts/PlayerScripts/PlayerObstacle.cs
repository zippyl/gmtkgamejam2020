﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObstacle : MonoBehaviour
{
    #region Unity functions

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            Debug.Log($"Obstacle {collision.name}");
            if (Input.GetKeyDown(KeyCode.Return))
            {
                AudioManager.Instance.Play("hit");
                collision.GetComponent<BaseEnemy>().Die();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Candle")
        {
            collision.GetComponent<CandleController>().SaveMe();
        }
    }

    #endregion Unity functions
}