﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class TriggerZone : MonoBehaviour
{
#pragma warning disable
    [SerializeField] private BaseEnemy enemy;
#pragma warning restore

    private new BoxCollider2D collider;

    private void Start()
    {
        collider = GetComponent<BoxCollider2D>();
        collider.offset = new Vector2(0, enemy.transform.position.y);
        collider.size = new Vector2(enemy.MovePoint.GetSub, 2f);
        transform.position = new Vector2(enemy.transform.position.x + (collider.size.x / 2), transform.position.y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            enemy.SetMoveToPlayerState(true, collision.transform);
            Debug.Log($"Emeny moving to player state {enemy.IsMoveToPlayer}");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            enemy.SetMoveToPlayerState(false, null);
            Debug.Log($"Emeny moving to player state {enemy.IsMoveToPlayer}");
        }
    }
}