﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [Serializable]
    public class AudioInfo
    {
        public string id;
        public AudioSource audioSource;
    }

#pragma warning disable
    [SerializeField] private List<AudioInfo> audioInfos;
#pragma warning restore

    public static AudioManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(Instance);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            InstanceManager.GetManager<GameManager>().EndGame(GameManager.EndGameType.Win);
        }
    }

    public void Play(string id)
    {
        audioInfos.Where(x => x.id == id).First().audioSource.Play();
    }
}