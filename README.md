# README #

Game “Candels problem” for GMTK Game Jam 2020

Stanislav Kokoshko - Unity Developer - ctacoh15l@gmail.com
Victoria Panchenko - 3D Modeller - vikapanchenko25@gmail.com 
Victor Koval - Level Designer - koval.victor.smith@gmail.com 
Eugene Dzyaniy - 2D Artist/Animator - e.dzyaniy@gmail.com 
Yuriy Nizhnik - Sound Designer - Lfdumal@gmail.com 

Contact - ctacoh15l@gmail.com